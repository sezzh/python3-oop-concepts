"""
Example of inheritance on python 3.6 using init and new magic methods.
"""

class ParentFoo(object):

    def __new__(cls, a, b):
        print('pase por new de ParentFoo')
        import ipdb; ipdb.set_trace()
        instance = object.__new__(cls)
        instance.val_parent = 'val ParentFoo'
        return instance

    def __init__(self, a, b):
      print('pase por init de ParentFoo')
      import ipdb; ipdb.set_trace()
      self.a = a
      self.b = b

    def get_a_plus_b(self):
      return '{} + {}'.format(self.a, self.b)

class MotherFoo(object):

    def __init__(self, d):
      print('pase por init de MotherFoo')
      self.d = d

    def get_d(self):
      return 'd value is: {}'.format(self.d)

class Foo(ParentFoo, MotherFoo):

    def __new__(cls, a, b, c, d):
        print('pase por new de Foo')
        import ipdb; ipdb.set_trace()
        instance = super().__new__(cls, a, b)
        instance.val_son = 'val Foo'
        return instance
 
    def __init__(self, a, b, c, d):
        import ipdb; ipdb.set_trace()
        print('pase por init de Foo')
        ParentFoo.__init__(self, a, b)
        MotherFoo.__init__(self, d)
        self.c = c
        import ipdb; ipdb.set_trace()
 
    def bar(self):
        pass

def main():
    holi = Foo('param a', 'param b', 'param c', 'param d')
    print(holi.get_a_plus_b())
    import ipdb; ipdb.set_trace()

if __name__ == '__main__':
    main()
